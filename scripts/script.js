/*
 * @author Leonard Woo
 */
"use strict";

function storageTheme() {
  var theme = localStorage.getItem('theme');
  if (theme == null) {
    theme = "light";
  } else {
    theme = Pikajs.getDarkMode()? "dark": "light";
  }
  localStorage.setItem('theme', theme);
}

window.addEventListener("load", (event) => {
  storageTheme();
  var theme = localStorage.getItem("theme");
  if (theme == "dark") {
    document.body.classList.add("dark");
  } else {
    document.body.classList.remove("dark");
  }

  const fingerprintEles = document.querySelectorAll(".pgp-fingerprint");
  fingerprintEles.forEach((e) => {
    Pikajs.splitContentWithParity(e.innerHTML, " ", "em", "odd", "even")
      .then(function (event) {
        e.innerHTML = event;
      })
      .catch(function (error) {
        console.log(error);
      });
  });

  const animaEles = document.querySelectorAll(".animated-btn");
  animaEles.forEach((e) => {
    Pikajs.splitContent(e.innerHTML, "", "em")
      .then(function (event) {
        e.innerHTML = "<span>" + event + "</span>";
      })
      .catch(function (error) {
        console.log(error);
      });
  });
  
});

const popupDialog = document.querySelector("#popup-parent");
// const pgpKey = document.querySelector("#pgpkey-dialog");

document.querySelector("#pgpkey").addEventListener("click", () => {
  // pgpKey.classList.replace('hidden', 'block');
  popupDialog.show();
});

function close() {
  // pgpKey.classList.replace('block', 'hidden');
  popupDialog.close();
}

document.querySelector("#pgpkey-dialog #popup-box-close button").addEventListener("click", () => {
  close();
});

document.querySelector("#popup-mark").addEventListener("click", () => {
  close();
});

const backToTop = document.getElementById("back-to-top");
const backToTopToggle= document.getElementById("back-to-top-toggle");
const scrollOffset = backToTop.dataset.scrollOffset;
document.body.onscroll = function() {
  if ( document.body.scrollTop > scrollOffset || document.documentElement.scrollTop > scrollOffset) {
    backToTop.classList.replace("hidden", "block");
  } else {
    backToTop.classList.replace("block", "hidden");
  }
}
backToTopToggle.onclick = function(e) {
  scrollToTop(200, 10);
}

function scrollToTop(duration, delay) {
  const scrollStep = -window.scrollY / (duration / delay)
  const scrollInterval = setInterval(function() {
    if (window.scrollY > 0) {
      window.scrollBy(0, scrollStep)
    } else {
      clearInterval(scrollInterval)
    }
  }, delay);
}
